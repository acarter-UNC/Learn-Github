import requests
import pprint

def get_package_info(package_name):
    url = f'https://pypi.org/pypi/{package_name}/json'
    response = requests.get(url)
    
    return response.json() if response.status_code == 200 else None

def list_dependencies(package_name):
    package_info = get_package_info(package_name)
    
    dependencies = package_info['info']['requires_dist'] if package_info else None
    
    print(f"Dependencies for package '{package_name}':")
    [print(f"  - {dep}") for dep in dependencies] if dependencies else print("  - None")

if __name__ == "__main__":
    package_name = input("Enter the package name: ")
    list_dependencies(package_name)


